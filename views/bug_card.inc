<%- BLOCK cardview -%>
<div class="card mb-2 bug-card<% FOR subcomponent IN entry.subcomponents %> <% subcomponent %><% END %> collapse show">
  <div class="card-header d-flex justify-content-between align-items-center">
    <h6 class="status-<%- entry.bug_severity | replace('\s', '-') | uri -%> card-title mb-0"><a class="link-success" href="https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=<% entry.bug_id | uri %>" target="_blank"><% entry.bug_id | html %></a></h6>
    <%- IF entry.assignee -%>
        <span class="badge text-bg-info opacity-75" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Assigned to <% entry.assignee | html_entity %>"><%- entry.assignee_initials | html -%></span>
    <%- END -%>
  </div>
  <div class="card-body">
    <% entry.short_desc | html %>
    <%- IF entry.qa_contact -%>
    <ul>
      <li>QA Contact: <% entry.qa_contact | html_entity %></li>
    </ul>
    <%- END -%>
  </div>
  <div class="card-footer text-body-secondary d-flex justify-content-between">
    <div>
    <span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Sponsorship">
    <%- SWITCH entry.sponsorship -%>
      <%- CASE [ '---', 'Seeking sponsor' ] -%>
        <span class="badge text-bg-info opacity-75 mb-0">Seeking</span>
      <%- CASE 'Seeking cosponsors' -%>
        <span class="badge text-bg-info opacity-75 mb-0"><%- entry.sponsorship | html -%></span>
      <%- CASE 'Sponsored' -%>
        <span class="badge text-bg-primary opacity-50 mb-0"><%- entry.sponsorship | html -%></span>
      <%- CASE 'Unsponsored' -%>
        <span class="badge text-bg-primary opacity-50 mb-0"><%- entry.sponsorship | html -%></span>
      <%- CASE 'Seeking developer' -%>
        <span class="badge text-bg-primary opacity-75 mb-0"><%- entry.sponsorship | html -%></span>
    <%- END -%>
    </span>
    </div>
    <div>
    <span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Difficulty">
    <%- SWITCH entry.complexity -%>
      <%- CASE '---' -%>
        <span class="badge text-bg-secondary">Not scoped</span>
      <%- CASE 'String patch' -%>
        <span class="badge text-bg-info opacity-75">String</span>
      <%- CASE 'Trivial patch' -%>
        <span class="badge text-bg-info opacity-75">Trivial</span>
      <%- CASE 'Small patch' -%>
        <span class="badge text-bg-primary opacity-50">Small</span>
      <%- CASE 'Medium patch' -%>
        <span class="badge text-bg-primary opacity-75">Medium</span>
      <%- CASE 'Large patch' -%>
        <span class="badge text-bg-primary">Large</span>
    <%- END -%>
    </span>
    <span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Priority">
    <%- SWITCH entry.priority -%>
      <%- CASE 'P1 - high' -%>
        <span class="badge text-bg-danger">High</span>
      <%- CASE ['P2', 'P3'] -%>
        <span class="badge text-bg-danger opacity-50">Medium</span>
      <%- CASE -%>
        <span class="badge text-bg-warning opacity-75">Low</span>
    <%- END -%>
    </span>
    <span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Type">
    <%- IF entry.bug_severity == 'new feature' -%>
    <span class="badge text-bg-success"><%- entry.bug_severity | lower | ucfirst | html -%></span>
    <%- ELSIF entry.bug_severity == 'enhancement' -%>
    <span class="badge text-bg-warning"><%- entry.bug_severity | lower | ucfirst | html -%></span>
    <%- ELSE -%>
    <span class="badge text-bg-danger"><%- entry.bug_severity | lower | ucfirst | html -%></span>
    <%- END -%>
    </span>
    </div>
  </div>
</div>
<%- END -%>
