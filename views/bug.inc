<%- BLOCK bugview -%>
  <li class="status-<%- entry.bug_severity | replace('\s', '-') | uri -%><%- IF realname && entry.last_active_user && entry.last_active_user != realname %> fw-bold<%- END -%>">
    <a class="link-success" href="https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=<% entry.bug_id | uri %>" target="_blank"><% entry.bug_id | html %></a><% IF entry.qa_contact %> + <% ELSE %> - <% END %><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="<ul><li>Date: <% entry.bug_when | html %></li><%- IF entry.reporter -%><li>Reporter: <% entry.reporter | html %></li><%- END -%><%- IF entry.assignee -%><li>Assignee: <% entry.assignee | html %></li><%- END -%><%- IF entry.qa_contact -%><li>QA Contact: <% entry.qa_contact | html %></li><%- END -%><li>Type: <% entry.bug_severity | html %></li><li>Component: <% entry.component_name | html %></li><% IF entry.last_active_user %><li>Last active user: <% entry.last_active_user | html %><% END %></li></ul>"><% entry.short_desc | html %></span> <%- IF entry.keywords -%>
        <%- FOREACH keyword IN entry.keywords -%>
          <%- IF keyword == 8 -%><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Academy"> &#127891;</span>
          <%- ELSIF keyword == 16 -%><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Release notes needed"> &#128221;</span>
          <%- ELSIF keyword == 18 -%><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Release manager priority"> &#128081;</span>
          <%- ELSIF keyword == 19 -%><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Additional work needed"> &#128295;</span>
          <%- ELSIF keyword == 42 OR keyword == 39 -%><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="Roadmap project"> &#127919;</span>
          <%- END -%>
        <%- END -%>
      <%- END -%>
  </li>
<%- END -%>
