  <nav class="navbar navbar-expand-lg fixed-top bg-dark p-2 text-white" data-bs-theme="dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="https://koha-community.org/"><img src="/assets/img/koha_128dpi.png" alt="Logo" height="24" class="d-inline-block align-text-top"> Koha</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="/">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="/leaderboard">Leaderboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="/roadmap">Roadmap</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="/components">Kanban</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="/maintenance">Maintenance</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="https://wiki.koha-community.org/">Wiki</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="https://bugs.koha-community.org/">Bugzilla</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="https://jenkins.koha-community.org/">Jenkins</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" aria-content="page" href="https://wiki.koha-community.org/wiki/Sandboxes">Sandboxes</a>
          </li>
        </ul>
        <form class="d-flex" role="search" action="/find_my_bugs">
          <input class="form-control me-2" type="search" placeholder="Find your dashboard" aria-label="Find your dashboard" name="name">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>
